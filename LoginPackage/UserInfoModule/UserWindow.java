package edu.baylor.csi.UserInfo.Lee;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.JFormattedTextField;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import java.awt.Font;

public class UserWindow extends JFrame {

    private JPanel contentPane;
    private final JLabel lblUsername = new JLabel("UserName:");
    private final JLabel lblHeight = new JLabel("Height:");
    private final JLabel lblWeight = new JLabel("Weight:");
    private final JFormattedTextField heightTextField = new JFormattedTextField();
    private final JFormattedTextField weightTextField = new JFormattedTextField();
    private final JTextPane userNameTextField = new JTextPane();
    private final JButton btnProfile = new JButton("Profile");
    private final JButton btnDiary = new JButton("Diary");
    private final JButton btnSchedule = new JButton("Schedule");
    private final JButton btnRoutine = new JButton("Routine");
    private final JButton btnOnerepmax = new JButton("One-Rep-Max");
    private final JComboBox comboBox = new JComboBox();
    private final JLabel lblCurrentMaxes = new JLabel("Current Maxes");
    private final JTextField textField = new JTextField();
    private final JLabel lblLbs = new JLabel("lbs");
    private final JLabel lblDietStatistics = new JLabel("Diet Statistics");
    private final JLabel lblCaloireIntake = new JLabel("Caloire Intake:");
    private final JTextField textField_1 = new JTextField();
    private final JLabel lblCals = new JLabel("cals");
    private final JLabel lblProteinfatcarbohydrates = new JLabel("Protein/Fat/Carbs:");
    private final JTextField textField_2 = new JTextField();
    private final JLabel lblGrams = new JLabel("grams");
    private final JLabel lblProfile = new JLabel("Profile");

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UserWindow frame = new UserWindow();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public UserWindow() {

        textField_2.setEditable(false);
        textField_2.setText("150/75/300");
        textField_2.setBounds(458, 135, 108, 29);
        textField_2.setColumns(10);
        textField_1.setEditable(false);
        textField_1.setText("2500");
        textField_1.setBounds(458, 109, 82, 21);
        textField_1.setColumns(10);
        textField.setEditable(false);
        textField.setText("265");
        textField.setBounds(63, 237, 130, 26);
        textField.setColumns(10);
        initGUI();
    }
    private void initGUI() {
        setFont(new Font("Dialog", Font.BOLD, 14));
        setTitle("Fitness.AIO");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 618, 430);
        contentPane = new JPanel();
        contentPane.setBorder(new LineBorder(new Color(0, 0, 0)));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        lblUsername.setBounds(63, 87, 77, 16);

        contentPane.add(lblUsername);
        lblHeight.setBounds(63, 112, 61, 16);

        contentPane.add(lblHeight);
        lblWeight.setBounds(63, 140, 61, 16);

        contentPane.add(lblWeight);
        heightTextField.setText("5'7\"");
        heightTextField.setBounds(136, 112, 77, 16);

        contentPane.add(heightTextField);
        weightTextField.setText("160");
        weightTextField.setBounds(136, 140, 77, 16);

        contentPane.add(weightTextField);
        userNameTextField.setEditable(false);
        userNameTextField.setText("thisnotbryan");
        userNameTextField.setBounds(152, 87, 96, 16);

        contentPane.add(userNameTextField);
        btnProfile.setBounds(0, 0, 117, 29);

        contentPane.add(btnProfile);
        btnDiary.setBounds(112, 0, 117, 29);

        contentPane.add(btnDiary);
        btnSchedule.setBounds(227, 0, 117, 29);

        contentPane.add(btnSchedule);
        btnRoutine.setBounds(342, 0, 117, 29);

        contentPane.add(btnRoutine);
        btnOnerepmax.setBounds(458, 0, 117, 29);

        contentPane.add(btnOnerepmax);
        comboBox.setModel(new DefaultComboBoxModel(new String[] {"Max Bench", "Max Squat", "Max Deadlift"}));
        comboBox.setBounds(63, 206, 141, 29);

        contentPane.add(comboBox);
        lblCurrentMaxes.setBounds(79, 186, 108, 16);

        contentPane.add(lblCurrentMaxes);

        contentPane.add(textField);
        lblLbs.setBounds(203, 242, 61, 16);

        contentPane.add(lblLbs);
        lblDietStatistics.setBounds(436, 87, 96, 16);

        contentPane.add(lblDietStatistics);
        lblCaloireIntake.setBounds(357, 112, 96, 16);

        contentPane.add(lblCaloireIntake);

        contentPane.add(textField_1);
        lblCals.setBounds(538, 112, 61, 16);

        contentPane.add(lblCals);
        lblProteinfatcarbohydrates.setBounds(330, 140, 129, 16);

        contentPane.add(lblProteinfatcarbohydrates);

        contentPane.add(textField_2);
        lblGrams.setBounds(568, 141, 44, 16);

        contentPane.add(lblGrams);
        lblProfile.setBounds(283, 24, 77, 35);

        contentPane.add(lblProfile);
        


    }
}

