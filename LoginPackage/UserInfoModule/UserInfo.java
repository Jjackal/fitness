package edu.baylor.csi.UserInfo.Lee;


public class UserInfo {
    private double bmi;
    private double curr_max_bench;
    private double curr_max_deadLift;
    private double curr_max_squat;
    private double calorieIntake;
    private boolean is_newUser;


    public boolean isIs_newUser() {
        return is_newUser;
    }

    public void setIs_newUser(boolean is_newUser) {
        this.is_newUser = is_newUser;
    }

    public double getBmi() {
        return bmi;
    }

    public void setBmi(double bmi) {
        this.bmi = bmi;
    }

    public double getCurr_max_bench() {
        return curr_max_bench;
    }

    public void setCurr_max_bench(double curr_max_bench) {
        this.curr_max_bench = curr_max_bench;
    }

    public double getCurr_max_deadLift() {
        return curr_max_deadLift;
    }

    public void setCurr_max_deadLift(double curr_max_deadLift) {
        this.curr_max_deadLift = curr_max_deadLift;
    }

    public double getCurr_max_squat() {
        return curr_max_squat;
    }

    public void setCurr_max_squat(double curr_max_squat) {
        this.curr_max_squat = curr_max_squat;
    }

    public double getCalorieIntake() {
        return calorieIntake;
    }

    public void setCalorieIntake(double calorieIntake) {
        this.calorieIntake = calorieIntake;
    }
}
