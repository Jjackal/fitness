package edu.baylor.csi.Lee;


import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;




public class LoginWindow extends JDialog {
    private final JPanel contentPanel = new JPanel();
    private JTextField username;
    private JPasswordField password;
    Login loginInfo = new Login();

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        try {
            LoginWindow dialog = new LoginWindow();
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the dialog.
     */
    public LoginWindow() {
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);
        {
            JLabel lblUsername = new JLabel("UserName");
            lblUsername.setBounds(89, 76, 67, 20);
            contentPanel.add(lblUsername);
        }
        {
            JLabel lblPassword = new JLabel("Password");
            lblPassword.setBounds(89, 119, 63, 20);
            contentPanel.add(lblPassword);
        }

        username = new JTextField();
        username.setBounds(173, 76, 152, 20);
        contentPanel.add(username);
        username.setColumns(10);

        password = new JPasswordField();
        password.setBounds(173, 119, 152, 20);
        contentPanel.add(password);

        JButton btnLogin = new JButton("Login");
        JButton btncreateUser = new JButton("Create New User");



        btnLogin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {

                loginInfo.setUserName(username.getText());
                loginInfo.setPassWord(password.getText());

                if (username.getText().equals("HELLO") && password.getText().equals("WORLD")) {
                    //JOptionPane.showMessageDialog(null, "Login Sucessful ");
                    dispose();
                    UserInfoWindow frame = new UserInfoWindow();
                    frame.setVisible(true);
                }else {
                    JOptionPane.showMessageDialog(null, "Wrong Inputs", "Please Check", JOptionPane.WARNING_MESSAGE);
                    return;
                }
            }
        });

        btncreateUser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("CREAT NEW USER ACTION");

            }
        });

        btnLogin.setBounds(100, 165, 90, 23);
        btncreateUser.setBounds(200, 165, 150, 23);

        contentPanel.add(btnLogin, BorderLayout.EAST);
        contentPanel.add(btncreateUser,BorderLayout.SOUTH);

        JLabel lblLogin = new JLabel("Fitness.AIO");
        lblLogin.setHorizontalAlignment(SwingConstants.CENTER);
        lblLogin.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblLogin.setBounds(104, 23, 230, 23);
        contentPanel.add(lblLogin);
    }
}