package edu.baylor.csi.Lee;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class CreateNewUserWindow extends JFrame {

    private JPanel contentPane;
    private final JLabel lblCreateNewUser = new JLabel("Create New User");
    private final JLabel lblUserName = new JLabel("User Name:");
    private final JTextField txtEnterNewName = new JTextField();
    private final JLabel lblPassword = new JLabel("Password:");
    private final JPasswordField passwordField = new JPasswordField();
    private final JLabel lblReenterPassword = new JLabel("Re-enter Password:");
    private final JPasswordField passwordField_1 = new JPasswordField();
    private final JLabel lblNamel = new JLabel("Name:");
    private final JTextField textField = new JTextField();
    private final JButton btnNewButton = new JButton("Create Account");

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    CreateNewUserWindow frame = new CreateNewUserWindow();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public CreateNewUserWindow() {
        textField.setBounds(262, 69, 130, 26);
        textField.setColumns(10);
        txtEnterNewName.setBounds(262, 109, 130, 26);
        txtEnterNewName.setColumns(10);
        initGUI();
    }
    private void initGUI() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 600, 420);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        lblCreateNewUser.setBounds(235, 6, 117, 24);

        contentPane.add(lblCreateNewUser);
        lblUserName.setBounds(142, 114, 108, 16);

        contentPane.add(lblUserName);

        contentPane.add(txtEnterNewName);
        lblPassword.setBounds(142, 166, 74, 16);

        contentPane.add(lblPassword);
        passwordField.setBounds(262, 161, 130, 24);

        contentPane.add(passwordField);
        lblReenterPassword.setBounds(85, 212, 141, 16);

        contentPane.add(lblReenterPassword);
        passwordField_1.setBounds(262, 207, 130, 21);

        contentPane.add(passwordField_1);
        lblNamel.setBounds(155, 74, 61, 16);

        contentPane.add(lblNamel);

        contentPane.add(textField);
        btnNewButton.setBounds(203, 274, 141, 29);

        contentPane.add(btnNewButton);
    }
}

