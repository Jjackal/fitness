package edu.baylor.csi.Lee;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnect {
    public static void main(String args[]) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            String url = "jdbc:mysql://127.0.0.1:3306/LoginDataBase";
            String user = "root";
            String dbPassword = "password";

            Connection con = DriverManager.getConnection(url, user, dbPassword);

        } catch( SQLException err){
            System.out.println(err.getMessage());
        }

    }
}
