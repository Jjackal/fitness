package edu.baylor.csi.OneRep.Lee;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFormattedTextField;
import javax.swing.JTextArea;

public class OneRepWindow extends JFrame {

    private JPanel contentPane;
    private final JButton btnProfile = new JButton("Profile");
    private final JButton btnDiary = new JButton("Diary");
    private final JButton btnSchedule = new JButton("Schedule");
    private final JButton btnRoutines = new JButton("Routines");
    private final JButton btnOnerepMax = new JButton("One-Rep Max");
    private final JLabel lblOneRepMax = new JLabel("One Rep Max");
    private final JLabel lblExercise = new JLabel("Exercise:");
    private final JComboBox comboBox = new JComboBox();
    private final JFormattedTextField formattedTextField = new JFormattedTextField();
    private final JLabel lblCurrentMax = new JLabel("Current Max:");
    private final JLabel lblNewMax = new JLabel("New Max:");
    private final JFormattedTextField frmtdtxtfldEnterNewMax = new JFormattedTextField();
    private final JButton btnCalculateNewMax = new JButton("Calculate New Max");
    private final JFormattedTextField formattedTextField_1 = new JFormattedTextField();
    private final JFormattedTextField formattedTextField_2 = new JFormattedTextField();
    private final JFormattedTextField formattedTextField_3 = new JFormattedTextField();
    private final JFormattedTextField formattedTextField_4 = new JFormattedTextField();
    private final JFormattedTextField formattedTextField_5 = new JFormattedTextField();
    private final JLabel lblMaxPercents = new JLabel("Max Percents:");
    private final JLabel label = new JLabel("50%");
    private final JLabel label_1 = new JLabel("60%");
    private final JLabel label_2 = new JLabel("65%");
    private final JLabel label_3 = new JLabel("75%");
    private final JLabel label_4 = new JLabel("85%");
    private final JTextArea txtrDirections = new JTextArea();

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    OneRepWindow frame = new OneRepWindow();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public OneRepWindow() {
        initGUI();
    }
    private void initGUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 600, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        btnProfile.setBounds(0, 0, 117, 29);

        contentPane.add(btnProfile);
        btnDiary.setBounds(114, 0, 117, 29);

        contentPane.add(btnDiary);
        btnSchedule.setBounds(228, 0, 117, 29);

        contentPane.add(btnSchedule);
        btnRoutines.setBounds(340, 0, 117, 29);

        contentPane.add(btnRoutines);
        btnOnerepMax.setBounds(456, 0, 117, 29);

        contentPane.add(btnOnerepMax);
        lblOneRepMax.setBounds(253, 41, 86, 16);

        contentPane.add(lblOneRepMax);
        lblExercise.setBounds(72, 77, 61, 16);

        contentPane.add(lblExercise);
        comboBox.setModel(new DefaultComboBoxModel(new String[] {"Bench", "Squat", "Dead Lift"}));
        comboBox.setBounds(43, 96, 117, 27);

        contentPane.add(comboBox);
        formattedTextField.setEditable(false);
        formattedTextField.setText("265");
        formattedTextField.setBounds(100, 125, 107, 29);

        contentPane.add(formattedTextField);
        lblCurrentMax.setBounds(17, 125, 86, 29);

        contentPane.add(lblCurrentMax);
        lblNewMax.setBounds(17, 158, 61, 29);

        contentPane.add(lblNewMax);
        frmtdtxtfldEnterNewMax.setText("Enter New Max");
        frmtdtxtfldEnterNewMax.setBounds(100, 158, 107, 29);

        contentPane.add(frmtdtxtfldEnterNewMax);
        btnCalculateNewMax.setBounds(16, 190, 175, 29);

        contentPane.add(btnCalculateNewMax);
        formattedTextField_1.setEditable(false);
        formattedTextField_1.setText("50%");
        formattedTextField_1.setBounds(17, 302, 86, 29);

        contentPane.add(formattedTextField_1);
        formattedTextField_2.setEditable(false);
        formattedTextField_2.setText("60%");
        formattedTextField_2.setBounds(114, 302, 86, 28);

        contentPane.add(formattedTextField_2);
        formattedTextField_3.setEditable(false);
        formattedTextField_3.setText("65%");
        formattedTextField_3.setBounds(228, 302, 91, 28);

        contentPane.add(formattedTextField_3);
        formattedTextField_4.setEditable(false);
        formattedTextField_4.setText("75%");
        formattedTextField_4.setBounds(340, 302, 86, 29);

        contentPane.add(formattedTextField_4);
        formattedTextField_5.setEditable(false);
        formattedTextField_5.setText("85%");
        formattedTextField_5.setBounds(451, 302, 86, 28);

        contentPane.add(formattedTextField_5);
        lblMaxPercents.setBounds(253, 237, 102, 16);

        contentPane.add(lblMaxPercents);
        label.setBounds(27, 281, 61, 16);

        contentPane.add(label);
        label_1.setBounds(130, 281, 61, 16);

        contentPane.add(label_1);
        label_2.setBounds(246, 281, 61, 16);

        contentPane.add(label_2);
        label_3.setBounds(357, 281, 61, 16);

        contentPane.add(label_3);
        label_4.setBounds(465, 281, 61, 16);

        contentPane.add(label_4);
        txtrDirections.setText("Directions:");
        txtrDirections.setEditable(false);
        txtrDirections.setBounds(357, 100, 214, 109);

        contentPane.add(txtrDirections);
    }
}
