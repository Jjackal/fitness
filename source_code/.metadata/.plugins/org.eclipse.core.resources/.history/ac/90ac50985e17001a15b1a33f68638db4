/**
 * Contains information regarding food eaten.
 * 
 * @author Jackson O'Donnell
 * @version 1.0
 */

package diary_objects;

import java.util.ArrayList;
import java.util.List;

import org.javatuples.Pair;

public class FoodDiary extends Diary{
	
	//Holds a list of food eaten
	private List<Pair<Food, Integer>> foodList;
	
	/**
	 * Default constructor for the Food Diary. Starts with nothing.
	 */
	public FoodDiary() {
		foodList = null;
	}
	
	/**
	 * Specific constructor for the Food Diary.
	 * 
	 * @param food A list of food to add to the list.
	 */
	public FoodDiary(String date, String note, ArrayList food) {
		super(date, note);
		foodList = new ArrayList<Pair<Food, Integer> >(food);
	}
	
	/**
	 * Adds a single unit of a food to the list.
	 * 
	 * @param f The type of food to add
	 */
	public void addOneFood(Food f) {
		for(Pair<Food, Integer> p : foodList) {
			if(p.getValue0().equals(f)) {
				int a = p.getValue1()+1;
				p.setAt1(a);
				return;
			}
		}
		foodList.add(new Pair(f, 1));
	}
	
	/**
	 * Adds {@code quantity} of a type of food to the list of foods.
	 * 
	 * @param f The type of food to be added
	 * @param quantity The quantity of food to be added
	 */
	public void addFood(Food f, Integer quantity) {
		for(Pair<Food, Integer> p : foodList) {
			if(p.getValue0().equals(f)) {
				int a = p.getValue1()+quantity;
				p.setAt1(a);
				return;
			}
		}
		foodList.add(new Pair(f, quantity));
	}
	
	
}
