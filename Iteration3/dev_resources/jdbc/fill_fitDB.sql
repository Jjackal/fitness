-- -----------------------------------------------------------
-- Author: Jackson O'Donnell
--
-- Purpose: Create dummy data for testing
-- -----------------------------------------------------------

connect 'jdbc:derby:.\suedeDB';

INSERT INTO users VALUES
    ('Bryan', 'Bryan Lee', 'password'),
    ('Jjackal', 'Jackson O\'Donnell', 'password'),
    ('ConcussionBoi', 'Trenton Strickland', 'lol');

INSERT INTO diary VALUES
    ('Bryan', '20/06/2019'),
    ('Bryan', '21/06/2019'),
    ('Bryan', '22/06/2019'),
    ('Bryan', '23/06/2019'),
    ('Bryan', '24/06/2019'),
    ('Jjackal', '01/02/2019'),
    ('ConcussionBoi', '02/12/2019');

INSERT INTO food_list VALUES
    (1, 'banana'),
    (1, 'apple'),
    (1, 'strudel'),
    (1, 'schnitzel'),
    (1, 'haggelslag'),
    (2, 'omlette du fromage'),
    (3, 'strudel'),
    (4, 'good eatin'),
    (5, 'food'),
    (6, 'apple'),
    (7, 'apple');
    
INSERT INTO food_notes VALUES
    (1), 'This is a note'),
    (2), 'note'),
    (3),
    (4), 'note'),
    (5), 'note'),
    (6), 'note'),
    (7), 'notes');
    
INSERT INTO sleep VALUES
    (1, 1, 'a note'),
    (2, 1, 'a note'),
    (3, 1, 'a note'),
    (4, 2, 'a note'),
    (5, 1, 'a note'),
    (6, 5, 'a note'),
    (7, 1, 'a note');
    
INSERT INTO food VALUES
    ('banana', 5, 5, 5, 5),
    ('apple', 5, 5, 5, 5),
    ('strudel', 5, 5, 5, 5),
    ('schnitzel', 5, 5, 5, 5),
    ('haggelslag', 5, 5, 5, 5),
    ('omlette du fromage', 5, 5, 5, 5),
    ('good eatin', 5, 5, 5, 5),
    ('food', 5, 5, 5, 5);
    
INSERT INTO work_out VALUES
    (1, 2),
    (2, 2),
    (3, 2),
    (4, 2),
    (5, 2),
    (6, 2),
    (7, 2);
    
VALUES INTO oneRep VALUES
    (1.0, 1.0, 1.0, 'Bryan', '20/06/2019'),
    (1.0, 1.0, 1.0, 'Bryan', '21/06/2019'),
    (1.0, 1.0, 1.0, 'Bryan', '22/06/2019'),
    (1.0, 1.0, 1.0, 'Bryan', '23/06/2019'),
    (1.0, 1.0, 1.0, 'Bryan', '24/06/2019'),
    (1.0, 1.0, 1.0, 'Jjackal', '01/02/2019'),
    (1.0, 1.0, 1.0, 'ConcussionBoi', '02/12/2019'),